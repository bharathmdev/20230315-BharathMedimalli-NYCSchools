//
//  SchoolListViewModelTests.swift
//  20230315-BharathMedimalli-NYCSchoolsTests
//
//  Created by bharath medimalli on 3/15/23.
//

import XCTest
@testable import NYCSchools

class SchoolListViewModelTests: XCTestCase {

    let viewModel = SchoolListViewModel()
    
    override func setUpWithError() throws {
        
    }
    
    func testViewModelService()
    {
        viewModel.fetchSchoolList()
        let expectation = XCTestExpectation()
       
       
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.3, execute:  {
            XCTAssertNotNil(self.viewModel.schoolLists)
            expectation.fulfill()
        })
        
         wait(for: [expectation], timeout: 7.0)
       
    }
    
    

    override func tearDownWithError() throws {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
    }

    func testExample() throws {
        // This is an example of a functional test case.
        // Use XCTAssert and related functions to verify your tests produce the correct results.
    }

    func testPerformanceExample() throws {
        // This is an example of a performance test case.
        self.measure {
            // Put the code you want to measure the time of here.
        }
    }

}
