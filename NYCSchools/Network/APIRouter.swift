//
//  APIRouter.swift
//  Assignment
//
//  Created by bharath medimalli on 03/15/2023.
//

import Alamofire
import Foundation

enum HTTPHeaderField: String {
    case contentType = "Content-Type"
}

// Custom Error object API Calls.
enum APIError: Error {
    case runtimeError(String)
}

// Request parameter types.
enum RequestParams {
    case body(_:Parameters)
}

enum ContentType: String {
    case json = "application/json"
}

/// Builds the URLRequest object.
enum APIRouter: URLRequestConvertible {
    case getSatScoreData
    
    
    // MARK: - HTTPMethod
    private var method: HTTPMethod {
        switch self {
        case .getSatScoreData:
            return .get
        }
    }
    
    // MARK: - url
    private var urlPath: String {
        switch self {
        case .getSatScoreData:
            return Urls.satScore
        }
    }
    
    // MARK: - Parameters
    private var parameters: RequestParams? {
        switch self {
        case .getSatScoreData:
            return nil
        }
    }
    
    func asURLRequest() throws -> URLRequest {
        let url = try urlPath.asURL()
        var urlRequest = URLRequest(url: url)
        // HTTP Method
        urlRequest.httpMethod = method.rawValue
        
        // Headers
        switch self {
        case .getSatScoreData:
            var headers = [String: String]()
            headers[HTTPHeaderField.contentType.rawValue] = ContentType.json.rawValue
            urlRequest.allHTTPHeaderFields = headers
        }
        
        // Parameters
        if let parameters = parameters {
            switch parameters {
            case .body(let params):
                urlRequest.httpBody = try JSONSerialization.data(withJSONObject: params, options: [])
            }
        }
        return urlRequest
    }
}
