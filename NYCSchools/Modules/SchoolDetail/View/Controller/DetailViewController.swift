//
//  DetailViewController.swift
//  NYCSchools
//
//  Created by bharath medimalli on 03/15/2023.
//
import UIKit

class DetailViewController: UIViewController {
    
    @IBOutlet private weak var schoolNameLabel: UILabel!
    @IBOutlet private weak var writingScoreLabel: UILabel!
    @IBOutlet private weak var mathScoreLabel: UILabel!
    @IBOutlet private weak var readingScoreLabel: UILabel!
    @IBOutlet private weak var totalTestTakersLabel: UILabel!
    
    // MARK: - Properties
    private var detailViewModel: DetailViewModel!
    
    // MARK: - View Life Cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        updateData()
    }
    
    func set(viewModel: DetailViewModel) {
        self.detailViewModel = viewModel
    }
    
    private func updateData() {
        self.title = detailViewModel.score.schoolName
        writingScoreLabel.text = detailViewModel.score.satWritingAvgScore
        mathScoreLabel.text = detailViewModel.score.satMathAvgScore
        readingScoreLabel.text = detailViewModel.score.satCriticalReadingAvgScore
        totalTestTakersLabel.text = detailViewModel.score.numOfSatTestTakers
        self.title = self.detailViewModel.score.schoolName
        self.navigationController?.navigationBar.titleTextAttributes =  [NSAttributedString.Key.foregroundColor:UIColor.black]
    }
}
