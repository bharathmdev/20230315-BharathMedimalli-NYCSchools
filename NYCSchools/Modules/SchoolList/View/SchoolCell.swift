//
//  SchoolCell.swift
//  NYCSchools
//
//  Created by bharath medimalli on 03/15/2023.
//

import UIKit

class SchoolCell: UITableViewCell {
    
    // MARK: - IBOutlet
    @IBOutlet weak var nameLabel: UILabel!
    
    // MARK: - override function
    override func awakeFromNib() {
        super.awakeFromNib()
        self.selectionStyle = .none
    }
    
    func set(score: SATScore) {
        nameLabel.text = score.schoolName?.capitalized
    }
    
    static var identifier: String {
        return "SchoolCell"
    }
}
