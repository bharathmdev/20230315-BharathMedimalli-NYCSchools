//  RestAPIClient.swift
//  Assignment
//
//  Created by bharath medimalli on 03/15/2023.
//

import Foundation
import Alamofire

/// This class is an interface to call the rest API.

protocol RestAPIClientProtocol {
    func request<T: Codable>(type: T.Type,
                                            route: APIRouter,
                                            completion:@escaping (Result < T,
                                                                           APIError>)
                                                -> Void)
}

class RestAPIClient: RestAPIClientProtocol {
    
    /// Generic method to make the API request and decode the data to generic type.
     func request<T: Codable>(type: T.Type,
                              route: APIRouter,
                              completion:@escaping (Result < T,APIError>)
                                                -> Void) {
        
        AF.request(route).response { response in
            let result = response.result
            switch result {
            case .success(let data):
                guard let data = data else {
                    completion(.failure(.runtimeError("No Proper data recieved")))
                    return
                }
                
                guard let obj = try? JSONDecoder().decode(T.self, from: data) else {
                    completion(.failure(.runtimeError("No Proper data recieved")))
                    return
                }
                completion(.success(obj))
            case .failure(let error):
                completion(.failure(.runtimeError(error.localizedDescription)))
            }
        }
       
    }
    
    func getSatScoreData(completion: @escaping (Result<[SATScore], APIError>) -> Void) {
        return request(type: [SATScore].self, route: APIRouter.getSatScoreData,
                       completion: completion)
        
    }
    
}
