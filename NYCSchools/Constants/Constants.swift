//
//  Constants.swift
//  NYCSchools
//
//  Created by bharath medimalli on 03/15/2023.
//

import Foundation

// MARK: - Urls
struct Urls {
    static let satScore = "https://data.cityofnewyork.us/resource/f9bf-2cp4.json"
}
