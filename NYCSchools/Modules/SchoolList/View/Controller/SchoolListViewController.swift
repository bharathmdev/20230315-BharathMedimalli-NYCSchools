//
//  SchoolListViewController.swift
//  NYCSchools
//
//  Created by bharath medimalli on 03/15/2023.
//

import UIKit

class SchoolListViewController: UIViewController {
    
    @IBOutlet weak var schoolListTableView: UITableView!
    let schoolListViewModel = SchoolListViewModel()
    

    override func viewDidLoad() {
        super.viewDidLoad()
        self.title = "Schools List"
        schoolListTableView.register(UINib(nibName: SchoolCell.identifier, bundle: nil), forCellReuseIdentifier: SchoolCell.identifier)
        schoolListTableView.dataSource = self
        schoolListTableView.delegate = self
        schoolListViewModel.delegate = self
        schoolListViewModel.fetchSchoolList()
        self.navigationController?.navigationBar.titleTextAttributes =  [NSAttributedString.Key.foregroundColor:UIColor.white]
        self.navigationController?.navigationBar.backgroundColor = UIColor.red
  
        
    }
}

extension SchoolListViewController: SchoolListViewModelDelegate {
    func updateView() {
        DispatchQueue.main.async {
            self.schoolListTableView.reloadData()
        }
    }

}


// MARK: - UITableViewDataSource
extension SchoolListViewController: UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return schoolListViewModel.schoolLists?.count ?? 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: SchoolCell.identifier, for: indexPath) as? SchoolCell else {
            fatalError()
        }
        
        if let score = schoolListViewModel.schoolLists?[indexPath.row] {
            cell.set(score: score)
        }
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    
    func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
}

// MARK: - UITableViewDelegate
extension SchoolListViewController: UITableViewDelegate {
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        guard let score = schoolListViewModel.schoolLists?[indexPath.row] else {
            return
        }
        guard let detailViewController = storyboard?.instantiateViewController(withIdentifier: "DetailViewController") as? DetailViewController else { fatalError() }
        detailViewController.set(viewModel: DetailViewModel(score: score))
        self.navigationController?.pushViewController(detailViewController,animated:true)
    }
}
