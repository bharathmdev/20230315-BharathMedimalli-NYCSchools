//
//  SchoolListViewModelProtocol.swift
//  NYCSchools
//
//  Created by bharath medimalli on 03/15/2023.
//

import Foundation

protocol SchoolListViewModelDelegate: AnyObject {
    func updateView()
}

