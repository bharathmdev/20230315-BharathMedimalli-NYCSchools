//
//  SchoolService.swift
//  NYCSchools
//
//  Created by bharath medimalli on 03/15/2023.
//

import Foundation

protocol SchoolsServiceProtocol : AnyObject {
    func fetchSchool(completion: @escaping (Result<[SATScore], APIError>) -> Void)
}

class SchoolService: SchoolsServiceProtocol {
    
    private let apiClinet: RestAPIClient
    
    init(apiClinet: RestAPIClient = RestAPIClient()) {
        self.apiClinet = apiClinet
    }
    
    // MARK: - Protocol Function
    func fetchSchool(completion: @escaping (Result<[SATScore], APIError>) -> Void) {
        apiClinet.getSatScoreData(completion: completion)
    }
}
