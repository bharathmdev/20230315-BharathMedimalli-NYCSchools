//
//  SchoolListViewModel.swift
//  NYCSchools
//
//  Created by bharath medimalli on 03/15/2023.
//

import Foundation

class SchoolListViewModel {
    
    var schoolLists: [SATScore]?
    weak var delegate: SchoolListViewModelDelegate?

    private var schoolsService: SchoolsServiceProtocol
    
    init(withService service: SchoolsServiceProtocol = SchoolService()) {
        self.schoolsService = service
    }
    
    
    // MARK: - Protocol function
    func fetchSchoolList() {
        LoadingIndicatorView.show("Loading")
        schoolsService.fetchSchool() { [weak self] result in
            LoadingIndicatorView.hide()
            switch result {
            case .success(let model):
                self?.schoolLists = model
                self?.delegate?.updateView()
            case .failure(let error):
                print(error.localizedDescription)
            }
            
        }
    }
}
